<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Project Issue Listing</title>
<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		
		#reglink {
			font-size: 10px;
		}
	
		</style>

</head>
<body>
<center>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
		</div>
	</div>
</center>
	<!-- end #header -->
	
<center>

<div class='simpleform' style='width:50%;'>
	

	<fieldset>
	<legend>Project Issue Listing</legend>

	
    <table>
 <center>
 <tr>

  <td width="250"><center>Features </br> <a href="<c:url value="addFeature.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
  <td width="250"><center>Tasks </br> <a href="<c:url value="addTask.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
  <td width="250"><center>Bugs </br> <a href="<c:url value="addBug.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
 </tr>
  </center>
 <c:forEach items="${project}" var="address">
  <tr>
   <td><c:out value="${project.features}" /></td>
   <td><c:out value="${project.tasks}" /></td>
   <td><c:out value="${project.bugs}" /></td>
   
  </tr>
 </c:forEach>
 
</table>

    </fieldset>

</div>

</center>
	
</body>
</html>

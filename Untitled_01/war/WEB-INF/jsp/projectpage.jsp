<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Project Issue Listing</title>
<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		
		#reglink {
			font-size: 10px;
		}
		table { float:left;width:200px;cellspacing:10}
 		#table_container{width:250px;margin:0 auto;}
	
		</style>

</head>
<body>
<center>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
		</div>
	</div>
</center>
	<!-- end #header -->
	
<center>

<div class='simpleform' style='width:50%;'>
	
	Welcome, <c:out value = "${model.sessiontest.username }"/>! (<a href="logout.htm" >Logout</a>)
	<fieldset>
	<legend>${model.projectname} Listing</legend>

	
<table>
 <center>
<tr>
 	<td width="250" colspan = "3" style ="text-align: center"><u>Features</u></td>
</tr>
  </center>
 <c:forEach items="${model.features}" var="cnt">
  <tr>
   <td colspan = "3" style ="text-align: center""><c:out value="${cnt.name}" /></td>
   
  </tr>
 </c:forEach>
  <tr>
  <td colspan = "3"><center><a href="<c:url value="addFeature.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
 </tr>
</table>
<table>
 <center>
<tr>
 	<td width="250" colspan = "3" style ="text-align: center"><u>Tasks</u></td>
</tr>
  </center>
 <c:forEach items="${model.tasks}" var="cnt">
  <tr>
   <td colspan = "3" style ="text-align: center""><c:out value="${cnt.name}" /></td>
   
  </tr>
 </c:forEach>
  <tr>
  <td colspan = "3"><center><a href="<c:url value="addFeature.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
 </tr>
</table>
<table>
 <center>
<tr>
 	<td width="250" colspan = "3" style ="text-align: center"><u>Bugs</u></td>
</tr>
  </center>
 <c:forEach items="${model.bugs}" var="cnt">
  <tr>
   <td colspan = "3" style ="text-align: center""><c:out value="${cnt.name}" /></td>
   
  </tr>
 </c:forEach>
  <tr>
  <td colspan = "3"><center><a href="<c:url value="addFeature.htm" />" ><img src="images/addnew.png" width="100" height="40"/></a></center></td>
 </tr>
</table>

</fieldset>

</div>

</center>
	
</body>
</html>

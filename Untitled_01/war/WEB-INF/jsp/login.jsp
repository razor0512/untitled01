<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Login</title>
<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		
		#reglink {
			font-size: 10px;
		}
	    .error { color: red; }
		
	
		</style>

</head>
<body>
<center>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
		</div>
	</div>
</center>
	<!-- end #header -->
	
<center>

<div class='simpleform' style='width:50%;'>
	<form:form method="post" commandName="loginUser">
	<form:errors path="*" cssClass="error"/>
	<fieldset>
	<legend>Login</legend>
	<p class='info'>
	 Please login with your username and password. <br />
	 Don't have an account?
	<a id="reglink" href="<c:url value="register.htm"/>">Sign up now!</a></p>
	
    <b>User Name:</b><br/>
    <form:input path="username" maxlength="60" /><br/>
    <b>Password:</b><br/>
    <form:password path="password" maxlength="60" /><br/>
    <input type="submit" value="Log In" />
    </fieldset>
    </form:form>
</div>

</center>
	
</body>
</html>

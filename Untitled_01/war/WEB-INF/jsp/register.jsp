<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>

<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		background-color: black;
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		.error { color: red; }
		</style>

</head>
<body>

	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
				
		</div>
	</div>
	<!-- end #header -->
	
	
<center>

<div class='simpleform' style='width:50%;'>
	
	<!-- <form action="#" method="post">
	<fieldset>
	<legend>User Registration</legend>
	<p class='info'>
	 Complete the form below to create an account!
	</p>
    <b>User Name:</b><br/>
    <input type="text" name="username" size="32" maxlength="60" /><br/>
    <b>Password:</b><br/>
    <input type="password" name="password" size="32" maxlength="60" /><br/>
    <b>Confirm Password:</b><br/>
    <input type="password" name="confirmPassword" size="32" maxlength="60" /><br/>
    <b>First Name:</b><br/>
    <input type="text" name="fname" size="32" maxlength="60" /><br/>
    <b>Middle Name:</b><br/>
    <input type="text" name="mname" size="32" maxlength="60" /><br/>
    <b>Last Name:</b><br/>
    <input type="text" name="lname" size="32" maxlength="60" /><br/>
    <b>Email Address:</b><br/>
    <input type="text" name="email" size="32" maxlength="60" /><br/>
    <input type="submit" value="Create Account" />
    </fieldset>
    </form> -->
	<form:form method="post" commandName="registerUser">
	<form:errors path="*" cssClass="error"/>
	<fieldset>
	<legend>User Registration</legend>
	<p class='info'>
	 Complete the form below to create an account!
	</p>
    <b>User Name:</b><br/>
    <form:input path="username" size="32" maxlength="60" /><br/>
    <b>Password:</b><br/>
    <form:password path="password" size="32" maxlength="60" /><br/>
    <b>Confirm Password:</b><br/>
    <form:password path="confpassword" size="32" maxlength="60" /><br/>
    <b>First Name:</b><br/>
    <form:input path="firstname" size="32" maxlength="60" /><br/>
    <b>Middle Name:</b><br/>
    <form:input path="middlename" size="32" maxlength="60" /><br/>
    <b>Last Name:</b><br/>
    <form:input path="lastname" size="32" maxlength="60" /><br/>
    <b>Email Address:</b><br/>
    <form:input path="email" size="32" maxlength="60" /><br/>
    <input type="submit" value="Create Account" />
    </fieldset>
	</form:form>
</div>

</center>
</body>
</html>
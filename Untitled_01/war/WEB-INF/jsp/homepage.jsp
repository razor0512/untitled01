<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<% if (session.getAttribute("sessionuservar") == null) { %>
<c:redirect url="/login.htm"/>
<% } %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Homepage</title>
<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		
		#reglink {
			font-size: 10px;
		}
	
		</style>


</head>
<body>
<center>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
		</div>
	</div>
</center>
	<!-- end #header -->
	
<center>
<div class='simpleform' style='width:50%;'>
<%--<form:form method="post" commandName="addProject">--%>
	<p style="color:#F00;"><c:out value="${model.message}" /></p>
	Welcome, <c:out value = "${model.sessiontest.username }"/>! (<a href="logout.htm" >Logout</a>)
	<fieldset>
	<legend>Project Listing</legend>
	    <table cellspacing = "10"> 
    	<tr>
    		<th>Project Name</th>
    		<th>Owner</th>
    		<th>&nbsp;</th>
    	</tr>
    	<c:forEach items="${model.projects}" var="cnt">
    		<c:url value="deleteProject.htm" var="deleteprojectURL">
    			<c:param name="projectid" value="${cnt.projectid }" />
    			<c:param name="owner" value="${cnt.owner }" />
    		</c:url>
    		<c:url value="projectpage.htm" var="projectpageURL">
    			<c:param name="projectid" value="${cnt.projectid }" />
    			<c:param name="projectname" value="${cnt.name }" />
    			<c:param name="owner" value="${cnt.owner }" />
    		</c:url>
    		<tr>
    			<td><a href="<c:out value="${projectpageURL}" />" >${cnt.name}</a></td>
    			<td><c:out value="${cnt.owner}" /></td>
    			<td><a href="<c:out value="${deleteprojectURL}" />" >Delete</a></td>
    		</tr>    		
    	</c:forEach>	
    	
    </table>
	<a href="<c:url value="addProject.htm" />"><img src="images/addnew.png" width="170" height="40"/></a></br>
	</fieldset>
<%--</form:form>--%>
</div>
</center>
</body>
</html>

<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<% if (session.getAttribute("sessionuservar") == null) { %>
<c:redirect url="/login.htm"/>
<% } %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Add Project</title>
<link rel="stylesheet" type="text/css" />
<link href="./style.css" rel="stylesheet" type="text/css" media="screen" />

<style type="text/css">
	  	body {
		margin: 30px;
		font: 15px normal Helvetica, Verdana, sans-serif;
		font-color: white;
		}

		a {
		color: white;
		background-color: transparent;
		font-size: 15px;
		}
		
		#reglink {
			font-size: 10px;
		}
	
		</style>


</head>
<body>
<center>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<h1>Untitled01</h1>
			</div>
		</div>
	</div>
</center>
	<!-- end #header -->
	
<center>

<div class='simpleform' style='width:50%;'>	
	<form:form method="post" commandName="addProject">
	<fieldset>
	<legend>Create New Project</legend>
    <b>Add new project</b><br/>
    <b>Name:</b><br>
    <form:input path="name" size="32" maxlength="60" /></br>
    <input type="submit" value="Add Project" />
    <a id="reglink" href="homepage.htm"><input type="button" value="Cancel" /></a>
    </fieldset>
    </form:form>
</div>

</center>

</body>
</html>
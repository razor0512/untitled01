--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accounts (
    username text,
    password text,
    userid bigint NOT NULL
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: accounts_userid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE accounts_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_userid_seq OWNER TO postgres;

--
-- Name: accounts_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE accounts_userid_seq OWNED BY accounts.userid;


--
-- Name: accounts_userid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('accounts_userid_seq', 26, true);


--
-- Name: bugs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bugs (
    name text,
    owner text,
    requester integer,
    projectid integer,
    bugid integer NOT NULL
);


ALTER TABLE public.bugs OWNER TO postgres;

--
-- Name: bugs_taskid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bugs_taskid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bugs_taskid_seq OWNER TO postgres;

--
-- Name: bugs_taskid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bugs_taskid_seq OWNED BY bugs.bugid;


--
-- Name: bugs_taskid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bugs_taskid_seq', 3, true);


--
-- Name: features; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE features (
    name text,
    owner text,
    requester integer,
    projectid integer,
    featureid integer NOT NULL
);


ALTER TABLE public.features OWNER TO postgres;

--
-- Name: features_featureid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE features_featureid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.features_featureid_seq OWNER TO postgres;

--
-- Name: features_featureid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE features_featureid_seq OWNED BY features.featureid;


--
-- Name: features_featureid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('features_featureid_seq', 3, true);


--
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE projects (
    projectname text,
    projectid integer NOT NULL,
    owner integer NOT NULL
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- Name: projects_projectid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE projects_projectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_projectid_seq OWNER TO postgres;

--
-- Name: projects_projectid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE projects_projectid_seq OWNED BY projects.projectid;


--
-- Name: projects_projectid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_projectid_seq', 15, true);


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tasks (
    name text,
    requester integer,
    projectid integer,
    taskid integer NOT NULL,
    owner text
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- Name: tasks_taskid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tasks_taskid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_taskid_seq OWNER TO postgres;

--
-- Name: tasks_taskid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tasks_taskid_seq OWNED BY tasks.taskid;


--
-- Name: tasks_taskid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tasks_taskid_seq', 4, true);


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    firstname text,
    middlename text,
    lastname text,
    email text,
    userid bigint NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: userid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts ALTER COLUMN userid SET DEFAULT nextval('accounts_userid_seq'::regclass);


--
-- Name: bugid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bugs ALTER COLUMN bugid SET DEFAULT nextval('bugs_taskid_seq'::regclass);


--
-- Name: featureid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY features ALTER COLUMN featureid SET DEFAULT nextval('features_featureid_seq'::regclass);


--
-- Name: projectid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY projects ALTER COLUMN projectid SET DEFAULT nextval('projects_projectid_seq'::regclass);


--
-- Name: taskid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks ALTER COLUMN taskid SET DEFAULT nextval('tasks_taskid_seq'::regclass);


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accounts (username, password, userid) FROM stdin;
razor0512	carbine123	18
archangel0512	carbine123	19
administrator	admin	26
\.


--
-- Data for Name: bugs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bugs (name, owner, requester, projectid, bugid) FROM stdin;
Bug 1	administrator	18	5	1
Bug 2	archangel0512	26	5	2
Bug 3	archangel0512	18	5	3
\.


--
-- Data for Name: features; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY features (name, owner, requester, projectid, featureid) FROM stdin;
Feature 1	administrator	18	5	1
Feature 2	archangel0512	26	5	2
Feature 3	razor0512	19	5	3
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects (projectname, projectid, owner) FROM stdin;
Project 2	5	19
Project Y	8	18
Project 3	11	19
Project 3	13	18
Project 500	15	18
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tasks (name, requester, projectid, taskid, owner) FROM stdin;
Task 1	19	5	2	archangel0512
Task 2	26	5	4	razor0512
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (firstname, middlename, lastname, email, userid) FROM stdin;
Kevin Eric	Ridao	Siangco	shdwstrider@gmail.com	18
asdf	dfsd	sfdasdf	dsadg	19
Major	Lee	Hung	shdwstrider@gmail.com	26
\.


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (userid);


--
-- Name: accounts_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_username_key UNIQUE (username);


--
-- Name: bugs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bugs
    ADD CONSTRAINT bugs_pkey PRIMARY KEY (bugid);


--
-- Name: features_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY features
    ADD CONSTRAINT features_pkey PRIMARY KEY (featureid);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (projectid);


--
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (taskid);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- Name: bugs_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bugs
    ADD CONSTRAINT bugs_owner_fkey FOREIGN KEY (owner) REFERENCES accounts(username) ON DELETE CASCADE;


--
-- Name: bugs_projectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bugs
    ADD CONSTRAINT bugs_projectid_fkey FOREIGN KEY (projectid) REFERENCES projects(projectid) ON DELETE CASCADE;


--
-- Name: bugs_requester_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bugs
    ADD CONSTRAINT bugs_requester_fkey FOREIGN KEY (requester) REFERENCES accounts(userid);


--
-- Name: features_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY features
    ADD CONSTRAINT features_owner_fkey FOREIGN KEY (owner) REFERENCES accounts(username) ON DELETE CASCADE;


--
-- Name: features_projectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY features
    ADD CONSTRAINT features_projectid_fkey FOREIGN KEY (projectid) REFERENCES projects(projectid) ON DELETE CASCADE;


--
-- Name: features_requester_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY features
    ADD CONSTRAINT features_requester_fkey FOREIGN KEY (requester) REFERENCES accounts(userid) ON DELETE CASCADE;


--
-- Name: projects_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_owner_fkey FOREIGN KEY (owner) REFERENCES accounts(userid) ON DELETE CASCADE;


--
-- Name: tasks_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_owner_fkey FOREIGN KEY (owner) REFERENCES accounts(username);


--
-- Name: tasks_projectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_projectid_fkey FOREIGN KEY (projectid) REFERENCES projects(projectid) ON DELETE CASCADE;


--
-- Name: tasks_requester_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_requester_fkey FOREIGN KEY (requester) REFERENCES accounts(userid);


--
-- Name: users_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_userid_fkey FOREIGN KEY (userid) REFERENCES accounts(userid) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


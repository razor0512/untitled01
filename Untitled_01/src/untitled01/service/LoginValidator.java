package untitled01.service;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import untitled01.domain.Login;
import untitled01.dao.LoginDao;
import untitled01.dao.JdbcLoginDao;

public class LoginValidator implements Validator {
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());
	private LoginManager loginManager;
    public LoginManager getLoginManager() {
		return loginManager;
	}

	public void setLoginManager(LoginManager loginManager) {
		this.loginManager = loginManager;
	}

	public boolean supports(Class clazz) {
        return Login.class.equals(clazz);
    }

    public void validate(Object obj, Errors errors) {
    	Login pi = (Login) obj;
        logger.info("Validating with " + pi + ": " + pi.getUsername());
        if(pi.getUsername() == "" || pi.getPassword() == "") {
        	if (pi.getUsername() == "")
        		errors.rejectValue("username", "error.no-username", "Value required.");   
        	if(pi.getPassword() == "") 
        		errors.rejectValue("password", "error.no-password", "Value required.");
        }
        else {
        	if(!loginManager.authenticateUser(pi)){
        		errors.rejectValue("password", "error.invalid", "Invalid username or password.");
        	}
        }
    }


}
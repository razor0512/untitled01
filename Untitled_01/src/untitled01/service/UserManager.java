package untitled01.service;

import java.io.Serializable;
import java.util.List;

import untitled01.dao.UserDao;
import untitled01.domain.User;

public interface UserManager extends Serializable{
	
	public List<User> getUser();
	public void setUserDao(UserDao userDao);
	public void addUser(User user);
	public int deleteUser(int id);
}

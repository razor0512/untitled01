package untitled01.service;

import java.io.Serializable;
import java.util.List;

import untitled01.domain.Login;
import untitled01.domain.SessionVarUser;

public interface LoginManager extends Serializable{
	public boolean authenticateUser(Login login);
	public List<SessionVarUser> getSessionVariables(Login login);

}

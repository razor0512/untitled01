package untitled01.service;

import java.io.Serializable;
import java.util.List;

import untitled01.domain.User;
//import untitled01.dao.UserDao;

public interface RegistrationManager extends Serializable{

	public boolean registerUser(User user);
	
}

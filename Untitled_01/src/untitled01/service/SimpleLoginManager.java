package untitled01.service;

import java.util.ArrayList;
import java.util.List;

import untitled01.domain.Login;
import untitled01.dao.JdbcLoginDao;
import untitled01.dao.LoginDao;
import untitled01.domain.SessionVarUser;

public class SimpleLoginManager implements LoginManager{
	private static final long serialVersionUID = 1L;
	private LoginDao loginDao;
	
	public SimpleLoginManager() {
		loginDao = new JdbcLoginDao();
	}

	public LoginDao getLoginDao() {
		return loginDao;
	}

	public void setLoginDao(LoginDao loginDao) {
		this.loginDao = loginDao;
	}

	public boolean authenticateUser(Login login) {
		return this.loginDao.authenticateUser(login);
	}
	
	public List<SessionVarUser> getSessionVariables(Login login) {
		return this.loginDao.getSessionVariables(login);
	}

}

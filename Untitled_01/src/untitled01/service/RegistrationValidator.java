package untitled01.service;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import untitled01.domain.User;
import untitled01.service.RegistrationManager;
public class RegistrationValidator implements Validator {
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());
	private RegistrationManager registrationManager;
	
	public RegistrationManager getRegistrationManager() {
		return registrationManager;
	}

	public void setRegistrationManager(RegistrationManager registrationManager) {
		this.registrationManager = registrationManager;
	}

	public boolean supports(Class clazz) {
        return User.class.equals(clazz);
    }

    public void validate(Object obj, Errors errors) {
    	User pi = (User) obj;
        if(pi.getUsername() == "" || pi.getPassword() == "" || pi.getConfpassword() == "" || pi.getEmail() == "" || pi.getFirstname() == "" || pi.getMiddlename() == "" || pi.getLastname() == "") 
        		errors.rejectValue("username", "error.register.emptyfield", "Value required.");   
        else {
        	if(pi.getPassword().compareTo(pi.getConfpassword()) != 0)
        		errors.rejectValue("password", "error.mismatch", "asdf");
        	else if (!registrationManager.registerUser(pi))
        		errors.rejectValue("username", "error.username-exists", "Username already exists.");	
        }
   
   }
}

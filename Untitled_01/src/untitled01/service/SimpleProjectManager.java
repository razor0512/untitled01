package untitled01.service;

import untitled01.dao.JdbcProjectDao;
import untitled01.dao.ProjectDao;
import untitled01.domain.Project;
import untitled01.domain.ProjectListing;

import java.util.List;

public class SimpleProjectManager implements ProjectManager{
	private static final long serialVersionUID = 1L;
	
	private ProjectDao projectDao;
	
	public SimpleProjectManager() {
		projectDao = new JdbcProjectDao();
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public List<Project> getProjects() {
		return this.projectDao.getProjectList();
	}

	public void addProject(Project project) {
		this.projectDao.addProject(project);
	}

	public void deleteProject(int projectid) {
		this.projectDao.deleteProject(projectid);
	}

	public List<ProjectListing> getTasks(int projectid) {
		return this.projectDao.getTasks(projectid);
	}
	
	public List<ProjectListing> getBugs(int projectid) {
		return this.projectDao.getBugs(projectid);
	}
	
	public List<ProjectListing> getFeatures(int projectid) {
		return this.projectDao.getFeatures(projectid);
	}}

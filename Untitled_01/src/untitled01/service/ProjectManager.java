package untitled01.service;

import java.io.Serializable;
import java.util.List;

import untitled01.domain.Project;
import untitled01.domain.ProjectListing;

public interface ProjectManager extends Serializable {

	public List<Project> getProjects();
	public void addProject(Project project);
	public void deleteProject(int projectid);
	public List<ProjectListing> getTasks(int projectid);
	public List<ProjectListing> getBugs(int projectid);
	public List<ProjectListing> getFeatures(int projectid);


}


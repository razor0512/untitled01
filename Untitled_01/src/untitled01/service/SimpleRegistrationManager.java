package untitled01.service;

import java.util.ArrayList;
import java.util.List;

import untitled01.domain.User;
import untitled01.dao.JdbcRegistrationDao;
import untitled01.dao.RegistrationDao;

public class SimpleRegistrationManager implements RegistrationManager{
	private static final long serialVersionUID = 1L;
	private RegistrationDao registrationDao;
	
	public SimpleRegistrationManager() {
		registrationDao = new JdbcRegistrationDao();
	}

	public RegistrationDao getRegistrationDao() {
		return registrationDao;
	}

	public void setRegistrationDao(RegistrationDao registrationDao) {
		this.registrationDao = registrationDao;
	}

	public boolean registerUser(User user) {
		return this.registrationDao.registerUser(user);
	}

}

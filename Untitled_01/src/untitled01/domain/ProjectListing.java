package untitled01.domain;

import java.io.Serializable;

public class ProjectListing implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String requester;
	private String owner;
	private int projectid;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getProjectid() {
		return projectid;
	}
	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	public String getRequester() {
		return requester;
	}
	public void setRequester(String requester) {
		this.requester = requester;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}

}

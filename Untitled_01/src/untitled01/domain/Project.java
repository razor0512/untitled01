package untitled01.domain;

import java.io.Serializable;

public class Project implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name, owner;
	private int projectid, ownerid;
	
	public void setOwnerid(int ownerid) {
		this.ownerid = ownerid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getProjectid() {
		return projectid;
	}
	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	public int getOwnerid() {
		return this.ownerid;
	}
	

}

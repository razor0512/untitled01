package untitled01.domain;

import java.io.Serializable;

public class AddProject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String projectName;
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	

}

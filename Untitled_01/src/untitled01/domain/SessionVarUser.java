package untitled01.domain;
import java.io.Serializable;


public class SessionVarUser implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private int userid;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
}

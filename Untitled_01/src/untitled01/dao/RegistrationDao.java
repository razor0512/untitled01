package untitled01.dao;

import java.util.List;

import untitled01.domain.User;

public interface RegistrationDao {

	public boolean registerUser(User user);

}

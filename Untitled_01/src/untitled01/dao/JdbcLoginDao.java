package untitled01.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import java.sql.*;

import untitled01.domain.Login;
import untitled01.domain.SessionVarUser;

public class JdbcLoginDao extends SimpleJdbcDaoSupport implements LoginDao {

	protected final Log Logger = LogFactory.getLog(getClass());
	public boolean authenticateUser(Login login){		
		String sql = "select count(*) from accounts where username = :username AND password = :password";
		MapSqlParameterSource paramsrc = new MapSqlParameterSource();
		paramsrc.addValue("username", login.getUsername());
		paramsrc.addValue("password", login.getPassword());
		int rowCount = getSimpleJdbcTemplate().queryForInt(sql,paramsrc);
		if(rowCount > 0)
			return true;
		else
			return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<SessionVarUser> getSessionVariables(Login login) {
		SessionVarUser sessionvars = new SessionVarUser();
		ResultSet rs;
		PreparedStatement pst;
		String sql = "SELECT username, userid FROM accounts WHERE username = ?";
		MapSqlParameterSource paramsrc = new MapSqlParameterSource();
		paramsrc.addValue("username", login.getUsername());
		return getJdbcTemplate().query(sql, new Object[] {login.getUsername()}, new SessionVarMapper());
	}
	
	public static class SessionVarMapper implements
	ParameterizedRowMapper<SessionVarUser> {

	public SessionVarUser mapRow(ResultSet rs, int arg1) throws SQLException {
	SessionVarUser sv = new SessionVarUser();	
	sv.setUsername(rs.getString("username"));
	sv.setUserid(rs.getInt("userid"));
	return sv;
	}

	}	
//	public static class SessionVarMapper implements
////		RowMapper<SessionVarUser> {
//			 ResultSetExtractor {
//
//			  @Override
//			  public Object extractData(ResultSet rs) throws SQLException {
//			    SessionVarUser sv = new SessionVarUser();
//			    rs.next();
//			    sv.setUsername(rs.getString(1));
//			    sv.setUserid(rs.getInt(2));
//			    return sv;
//			  }
//
//			} 

//		public SessionVarUser mapRow(ResultSet rs, int arg1) throws SQLException {
//		SessionVarUser sessionVarUser = new SessionVarUser();
//		sessionVarUser.setUserid(rs.getInt("userid"));
//		sessionVarUser.setUsername(rs.getString("username"));
//		return sessionVarUser;
//	}

	}



package untitled01.dao;

import java.util.List;

import untitled01.domain.Login;
import untitled01.domain.SessionVarUser;

public interface LoginDao {

	public boolean authenticateUser(Login login);
	public List<SessionVarUser> getSessionVariables(Login login);
}

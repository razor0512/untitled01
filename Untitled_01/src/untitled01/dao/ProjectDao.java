package untitled01.dao;

import java.util.List;

import untitled01.domain.Project;
import untitled01.domain.ProjectListing;

public interface ProjectDao {
	public List<Project> getProjectList();
	public void addProject(Project project);
	public void deleteProject(int projectid);
	public List<ProjectListing> getTasks(int projectid);
	public List<ProjectListing> getBugs(int projectid);
	public List<ProjectListing> getFeatures(int projectid);


}

package untitled01.dao;

import java.util.List;

import untitled01.domain.User;

public interface UserDao {

	public List<User> getUserList();

	public void addUser(User user);
	
	public void saveUser(User user);
	
	public int deleteUser(int id);

}
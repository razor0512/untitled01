package untitled01.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import untitled01.domain.Project;
import untitled01.domain.ProjectListing;

public class JdbcProjectDao extends SimpleJdbcDaoSupport implements ProjectDao {

	public List<Project> getProjectList() {
		List<Project> projects = getSimpleJdbcTemplate().query(
				"SELECT projectname,username,projectid FROM accounts,projects WHERE projects.owner = accounts.userid;", new ProjectMapper());
		return projects;
	}
	
	public void addProject(Project project) {
		String sql = "INSERT INTO public.projects(projectname,owner) VALUES(:projectname, :owner)";
		MapSqlParameterSource paramsrc = new MapSqlParameterSource();
		paramsrc.addValue("projectname", project.getName());
		paramsrc.addValue("owner", project.getOwnerid());
		logger.info(paramsrc.toString());
		int count = getSimpleJdbcTemplate().update(sql, paramsrc);
		logger.info("Rows affected: " + count);
	}

	public void deleteProject(int projectid) {
		String sql = "DELETE FROM public.projects WHERE projectid = :projectid";
		MapSqlParameterSource paramsrc = new MapSqlParameterSource();
		paramsrc.addValue("projectid",projectid);
		int count = getSimpleJdbcTemplate().update(sql, paramsrc);
		logger.info("Rows affected: " + count);		
	}

	public List<ProjectListing> getTasks(int projectid) {
		List<ProjectListing> tasks = getSimpleJdbcTemplate().query(
				"SELECT name,owner,accounts.username AS requester,projectid,taskid FROM tasks,accounts WHERE tasks.requester = accounts.userid AND tasks.projectid=" + projectid, new ProjectListMapper());
		return tasks;
	}
	
	public List<ProjectListing> getBugs(int projectid) {
		List<ProjectListing> bugs = getSimpleJdbcTemplate().query(
				"SELECT name,owner,accounts.username AS requester,projectid,bugid FROM bugs,accounts WHERE bugs.requester = accounts.userid AND bugs.projectid=" + projectid, new ProjectListMapper());
		return bugs;
	}
	
	public List<ProjectListing> getFeatures(int projectid) {
		List<ProjectListing> features = getSimpleJdbcTemplate().query(
				"SELECT name,owner,accounts.username AS requester,projectid,featureid FROM features,accounts WHERE features.requester = accounts.userid AND features.projectid=" + projectid, new ProjectListMapper());
		return features;
	}
	
	public static class ProjectMapper implements
	ParameterizedRowMapper<Project> {
		public Project mapRow(ResultSet rs, int arg1) throws SQLException {
			Project project = new Project();
			project.setName(rs.getString("projectname"));
			project.setOwner(rs.getString("username"));
			project.setProjectid(rs.getInt("projectid"));
			return project;
		}
	}

	public static class ProjectListMapper implements
	ParameterizedRowMapper<ProjectListing> {
		public ProjectListing mapRow(ResultSet rs, int arg1) throws SQLException {
			ProjectListing task = new ProjectListing();
			task.setName(rs.getString("name"));
			task.setOwner(rs.getString("owner"));
			task.setRequester(rs.getString("requester"));
			return task;
		}
	}

}

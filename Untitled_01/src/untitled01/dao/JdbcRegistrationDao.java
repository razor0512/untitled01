package untitled01.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.*;

import untitled01.domain.User;

public class JdbcRegistrationDao extends SimpleJdbcDaoSupport implements RegistrationDao {

	protected final Log Logger = LogFactory.getLog(getClass());
	
	public boolean registerUser(final User user){
		String sql0 = "select count(*) from accounts where username = :username";
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("username", user.getUsername());
		int rowCount = getSimpleJdbcTemplate().queryForInt(sql0,param);
		if(rowCount > 0)
			return false;
		
		final String INSERT_SQL = "INSERT INTO public.accounts VALUES(?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
		        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		            PreparedStatement ps = connection.prepareStatement(INSERT_SQL, new String[] {"userid"});
		            ps.setString(1, user.getUsername());
		            ps.setString(2, user.getPassword());
		            return ps;
		        }
		    },
		keyHolder);
			
		logger.info("Saving User:" + user.getUsername());
		String sql = "INSERT INTO public.users VALUES(:firstname, :middlename, :lastname, :email, :userid)";
		MapSqlParameterSource paramsrc = new MapSqlParameterSource();
		paramsrc.addValue("firstname", user.getFirstname());
		paramsrc.addValue("middlename", user.getMiddlename());
		paramsrc.addValue("lastname", user.getLastname());
		paramsrc.addValue("email", user.getEmail());
		paramsrc.addValue("userid", keyHolder.getKey());
		logger.info(paramsrc.toString());
		int count = getSimpleJdbcTemplate().update(sql, paramsrc);
		logger.info("Rows affected: " + count);
		if(count != 0)
			return true;
		else
			return false;				
		
      }
        

}

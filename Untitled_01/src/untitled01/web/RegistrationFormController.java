package untitled01.web;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

//import untitled01.dao.LoginDao;
//import untitled01.dao.JdbcLoginDao;
import untitled01.domain.User;
import untitled01.service.RegistrationManager;
import untitled01.service.SimpleRegistrationManager;

public class RegistrationFormController extends SimpleFormController{
	
	private RegistrationManager registrationManager;
	private User user;
	
	public ModelAndView onSubmit(Object command) throws ServletException{
		user = (User) command;
		return new ModelAndView(new RedirectView(getSuccessView()));

			
	}

	public RegistrationManager getRegistrationManager() {
		return registrationManager;
	}

	public void setRegistrationManager(RegistrationManager registrationManager) {
		this.registrationManager = registrationManager;
	}
	
}

package untitled01.web;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import untitled01.service.ProjectManager;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HomeController implements Controller {

    protected final Log logger = LogFactory.getLog(getClass());
    private ProjectManager projectManager;
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	if(request.getSession().getAttribute("sessionuservar") == null)
            return new ModelAndView(new RedirectView("login.htm"));	


    	String message = request.getParameter("message");
		if(message == null)
			message = "";
		String now = (new Date()).toString();
		logger.info("Returning hello view with "+ now);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("now", now);
		myModel.put("projects", this.projectManager.getProjects());
		myModel.put("message", message);
		myModel.put("sessiontest", request.getSession().getAttribute("sessionuservar"));
		
        return new ModelAndView("homepage", "model", myModel);
    }
    
	public ProjectManager getProjectManager() {
		return projectManager;
	}
	
	public void setProjectManager(ProjectManager projectManager) {
		this.projectManager = projectManager;
	}

}

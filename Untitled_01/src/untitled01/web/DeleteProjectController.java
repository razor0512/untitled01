package untitled01.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import untitled01.domain.SessionVarUser;
import untitled01.service.ProjectManager;

public class DeleteProjectController implements Controller{
	protected final Log logger = LogFactory.getLog(getClass());
	private ProjectManager projectManager;

	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String  param= request.getParameter("projectid");
		int id = Integer.parseInt(param);
		String owner = request.getParameter("owner");
		SessionVarUser sessionvar = (SessionVarUser)request.getSession().getAttribute("sessionuservar");
		if(owner.compareTo(sessionvar.getUsername())!= 0){
			Map<String, Object> myModel = new HashMap<String, Object>();		
			myModel.put("message", "Only the owner can delete that project!");	
			return new ModelAndView("deleteproject","model",myModel);
		}
				
		projectManager.deleteProject(id);
		
		Map<String, Object> myModel = new HashMap<String, Object>();		
		myModel.put("message", " Project Deleted.");	
		
		return new ModelAndView("deleteproject","model",myModel);		
	}

	public ProjectManager getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(ProjectManager projectManager) {
		this.projectManager = projectManager;
	}
	
	
}

package untitled01.web;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import untitled01.domain.Project;
import untitled01.domain.SessionVarUser;
import untitled01.service.ProjectManager;;

public class AddProjectFormController extends SimpleFormController {
	private ProjectManager projectManager;
	private Project project;
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    	if(request.getSession().getAttribute("sessionuservar") == null)
            return new ModelAndView(new RedirectView("login.htm"));	
		project = (Project) command;		
		SessionVarUser sessionvar = (SessionVarUser)request.getSession().getAttribute("sessionuservar");
		project.setOwnerid(sessionvar.getUserid());
		projectManager.addProject(project);
        return new ModelAndView(new RedirectView(getSuccessView()));	
			
	}

	public ProjectManager getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(ProjectManager projectManager) {
		this.projectManager = projectManager;
	}

}

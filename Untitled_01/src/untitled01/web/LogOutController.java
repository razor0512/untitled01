package untitled01.web;

import java.io.IOException;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class LogOutController implements Controller {

    protected final Log logger = LogFactory.getLog(getClass());
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	if(request.getSession().getAttribute("sessionuservar") == null)
            return new ModelAndView(new RedirectView("login.htm"));	

    	else {
    		request.getSession().removeAttribute("sessionuservar");
    		request.getSession().invalidate();
    		return new ModelAndView(new RedirectView("login.htm"));	
    	}
    }
    

}

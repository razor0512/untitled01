package untitled01.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import untitled01.domain.Login;
import untitled01.service.LoginManager;
import untitled01.service.SimpleLoginManager;
import untitled01.domain.SessionVarUser;

public class LoginFormController extends SimpleFormController{
	
	private LoginManager loginManager;
	private Login login;

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		login = (Login) command;
		SessionVarUser sv = new SessionVarUser();
		sv.setUserid(loginManager.getSessionVariables(login).get(0).getUserid());
		sv.setUsername(loginManager.getSessionVariables(login).get(0).getUsername());
        request.getSession().setAttribute("sessionuservar", sv);
        return new ModelAndView(new RedirectView(getSuccessView()));	
        
	}
	
//	public Object formBackingObject(HttpServletRequest request) {
//		return 
//	}

	
	public void setLoginManager(LoginManager loginManager){
		this.loginManager = loginManager;
	}
	
	public LoginManager getLoginManager(){
		return this.loginManager;
	}	

}

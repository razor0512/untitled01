package untitled01.web;

import org.springframework.web.servlet.ModelAndView;

import untitled01.web.WelcomeController;

import junit.framework.TestCase;

public class LoginControllerTests extends TestCase {

    public void testHandleRequestView() throws Exception{		
        WelcomeController controller = new WelcomeController();
        ModelAndView modelAndView = controller.handleRequest(null, null);		
        assertEquals("login", modelAndView.getViewName());
    }
}
